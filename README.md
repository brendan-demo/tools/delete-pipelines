# WARNING.  This is super distructive and will delete all pipelines from your project

## Usage
1. Clone this repository
1. Run `npm i`
1. Copy `.env.example` to `.env`
1. Set the varaibles (`GITLAB_TOKEN` and `PROJECT_ID`)
1. Run `node app.js`
1. Say bye bye to all your pipelines

## Other notes
* This package assumes you're using `gitlab.com` but you can change the baseURL in `app.js` and use it for a self-managed instance
* As it says in the LICENSE: 

```
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
```

Please just be careful 😃