require('dotenv').config()
const axios = require('axios');
const ora = require('ora');
const spinner = ora('🦊 Loading pipelines').start();

const baseURL   = "https://gitlab.com/api/v4/";
const token     = process.env.GITLAB_TOKEN;
const projectId = process.env.PROJECT_ID;

// TODO: Ensure GITLAB_TOKEN and PROJECT_ID

const gitlab = axios.create({
    baseURL: baseURL,
    headers: {'Private-Token': token}
  });

// Get all pipelines for the project
gitlab
    .get(`/projects/${projectId}/pipelines/`)
    .then(res => {
        spinner.color = 'red';
        spinner.text = `❗ About to delete ${res.data.length} pipelines ❗`
        setTimeout(() => {
            deleteAllPipelines(res.data)
        }, 2000);
    })
    .catch(e => {
        errorOut(e, `Error getting pipelines for project ${projectId}`)
    })

function deleteAllPipelines(list) {
    spinner.text = `❌ Deleting all ${list.length} pipelines ❌`
    setTimeout(() => {
        list.forEach(element => {
            delPipeline(element.id)
        });

        spinner.succeed("All pipelines deleted 👋")
    }, 2000);
    
}

function delPipeline(pipeId) {
    gitlab.delete(`/projects/${projectId}/pipelines/${pipeId}`)
    .catch(e => errorOut(e, `Error deleting pipeline ${pipeId}`));
}

function errorOut(error, message) {
    console.error(error);
    spinner.fail(message)
}